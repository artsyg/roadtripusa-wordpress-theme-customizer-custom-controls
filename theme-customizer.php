<?php
new theme_customizer();

class theme_customizer
{
    public function __construct()
    {
        //add_action ('admin_menu', array(&$this, 'customizer_admin'));
        add_action( 'customize_register', array(&$this, 'customize_manager' ));
    }

    /**
     * Add the Customize link to the admin menu
     * @return void
     */
    public function customizer_admin() {
        add_theme_page( 'Customize', 'Customize', 'edit_theme_options', 'customize.php' );
    }

    /**
     * Customizer manager demo
     * @param  WP_Customizer_Manager $wp_manager
     * @return void
     */
    public function customize_manager( $wp_manager )
    {
        $this->section( $wp_manager );
        //$this->custom_sections( $wp_manager );
    }

    public static function social_array() {
        $social_sites = array(
            'twitter'       => 'artsygeek_twitter_profile',
            'facebook'      => 'artsygeek_facebook_profile',
            'google-plus'   => 'artsygeek_googleplus_profile',
            'linkedin'      => 'artsygeek_linkedin_profile',
            'pinterest'     => 'artsygeek_pinterest_profile',
            'instagram'     => 'artsygeek_instagram_profile',
        );

        return apply_filters( 'social_array_filter', $social_sites );
    }

    /**
     * Add theme sections and panels
     *
     * @param  Obj $wp_manager - WP Manager
     *
     * @return Void
     */
    private function section( $wp_manager )
    {
        // Create theme options section
        $wp_manager->add_panel( 'rtusa_theme_customizer', array(
            'priority'       => 10,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title'          =>  __( 'Road Trip USA', 'artsygeek' ),
            'description' => __( 'Customizable options for roadtripusa.com.', 'artsygeek' )
        ) );

        /**
         * Add header image
         */
        $wp_manager->add_section( 'rtusa_header_background_image', array(
            'priority'       => 1,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title'          =>  __( 'Header Image', 'artsygeek' ),
            'description' => __( 'Customize the sitewide default header image', 'artsygeek' ),
            'panel'     => 'rtusa_theme_customizer',

        ) );

         // WP_Customize_Background_Image_Control
        $wp_manager->add_setting( 'rtusa_header_background', array(
            'default'        => '',
        ) );

        $wp_manager->add_control( new WP_Customize_Image_Control( $wp_manager, 'rtusa_header_background', array(
            'label'   => 'Background Image Setting',
            'section' => 'rtusa_header_background_image',
            'settings'   => 'rtusa_header_background',
            'priority' => 9
        ) ) );

        // WP_Customize_Background_Image_Control
        $wp_manager->add_setting( 'rtusa_header_background', array(
            'default'        => '',
        ) );

        $wp_manager->add_control( new WP_Customize_Background_Image_Control( $wp_manager, 'rtusa_header_background', array(
            'label'   => 'Background Image Setting',
            'section' => 'rtusa_header_background_image',
            'settings'   => 'rtusa_header_background',
            'priority' => 9
        ) ) );
       

        /**
         * Add default post thumbnail images
         */
        $wp_manager->add_section( 'rtusa_default_post_thumbnail', array(
            'priority'       => 1,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title'          =>  __( 'Default Featured Images', 'artsygeek' ),
            'description' => __( 'Customize the sitewide default featured images.', 'artsygeek' ),
            'panel'     => 'rtusa_theme_customizer',

        ) );

        // WP_Customize_Image_Control
        $wp_manager->add_setting( 'rtusa_default_thumbnail_wide', array(
            'default'        => '',
        ) );

        $wp_manager->add_control( new WP_Customize_Image_Control( $wp_manager, 'rtusa_default_thumbnail_wide', array(
            'label'   => 'Default Wide Featured Image',
            'section' => 'rtusa_default_post_thumbnail',
            'settings'   => 'rtusa_default_thumbnail_wide',
            'priority' => 2
        ) ) );

        // WP_Customize_Image_Control
        $wp_manager->add_setting( 'rtusa_default_thumbnail_regular', array(
            'default'        => '',
        ) );
        
        $wp_manager->add_control( new WP_Customize_Image_Control( $wp_manager, 'rtusa_default_thumbnail_regular', array(
            'label'   => 'Default Featured Image',
            'section' => 'rtusa_default_post_thumbnail',
            'settings'   => 'rtusa_default_thumbnail_regular',
            'priority' => 0
        ) ) );


        /**
         * Social Media Callout Button
         */        
         $wp_manager->add_section( 'rtusa_footer_social_button', array(
            'priority'       => 25,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title'          =>  __( 'Social Media Links', 'artsygeek' ),
            'description' => __( 'Add links to a social media service.', 'artsygeek' ),
            'panel'     => 'rtusa_theme_customizer'

        ) );


        $social_sites = $this->social_array();
       
        $priority = 5;

        // create a setting and control for each social site
        foreach ( $social_sites as $social_site => $value ) {
            
            $label = ucfirst( $social_site );
     
            if ( $social_site == 'google-plus' ) {
                $label = 'Google Plus';
            } elseif ( $social_site == 'linkedin' ) {
                $label = 'LinkedIn';
            } elseif ( $social_site == 'instagram' ) {
                $label = 'Instagram';
            } elseif ( $social_site == 'pinterest' ) {
                $label = 'Pinterest';
            } 

            // setting
            $wp_manager->add_setting( $social_site, array(
                'sanitize_callback' => 'esc_url_raw'
            ) );
            // control
            $wp_manager->add_control( $social_site, array(
                'type'     => 'url',
                'label'    => $label,
                'section'  => 'rtusa_footer_social_button',
                'priority' => $priority
            ) );

            // Textbox control
            $wp_manager->add_setting( $social_site . '_text', array(
                'default'        => __( $label, 'artsygeek' ),
            ) );

            $wp_manager->add_control( $social_site . '_text', array(
                'label'   => __( $label . ' Title', 'artsygeek' ),
                'section' => 'rtusa_footer_social_button',
                'type'    => 'text',
                'priority' => $priority,
                'sanitize_callback' => 'esc_textarea'
            ) );
            // increment the priority for next site
            $priority = $priority + 5;
        }

         /**
          * Legal link
         */
        
         $wp_manager->add_section( 'rtusa_footer_legal_link', array(
            'priority'       => 35,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title'          =>  __( 'Legal, Privacy and Copyright', 'artsygeek' ),
            'description' => __( 'Add a link to your site\'s privacy and terms of use page; set the copyright name.', 'artsygeek' ),
            'panel'     => 'rtusa_theme_customizer'

        ) );

        // Copyright text control
        $wp_manager->add_setting( 'rtusa_footer_copyright_text', array(
            'default'        => __( 'Road Trip USA', 'artsygeek' ),
        ) );

        $wp_manager->add_control( 'rtusa_footer_copyright_text', array(
            'label'   => __( 'Footer Copyright', 'artsygeek' ),
            'section' => 'rtusa_footer_legal_link',
            'type'    => 'text',
            'priority' => 0,
            'sanitize_callback' => 'esc_textarea'
        ) );

        // Legal page control
        $wp_manager->add_setting( 'rtusa_footer_legal_text', array(
            'default'        => __( 'Terms of Use', 'artsygeek' ),
        ) );

        $wp_manager->add_control( 'rtusa_footer_legal_text', array(
            'label'   => __( 'Footer Legal Link Text', 'artsygeek' ),
            'section' => 'rtusa_footer_legal_link',
            'type'    => 'text',
            'priority' => 1,
            'sanitize_callback' => 'esc_textarea'
        ) );

        // Legal page dropdown pages control
        $wp_manager->add_setting( 'rtusa_footer_legal_link', array(
            'default'        => '',
        ) );

        $wp_manager->add_control( 'rtusa_footer_legal_link', array(
            'label'   => __( 'Legal Page to Link To', 'artsygeek' ),
            'section' => 'rtusa_footer_legal_link',
            'type'    => 'url',
            'priority' => 2,
            'sanitize_callback' => 'esc_url_raw'
        ) );

         // Privacy page control
        $wp_manager->add_setting( 'rtusa_footer_privacy_text', array(
            'default'        => __( 'Privacy Policy', 'artsygeek' ),
        ) );

        $wp_manager->add_control( 'rtusa_footer_privacy_text', array(
            'label'   => __( 'Footer Privacy Link Text', 'artsygeek' ),
            'section' => 'rtusa_footer_legal_link',
            'type'    => 'text',
            'priority' => 3,
            'sanitize_callback' => 'esc_textarea'
        ) );

        // Dropdown pages control
        $wp_manager->add_setting( 'rtusa_footer_privacy_link', array(
            'default'        => '1',
        ) );

        $wp_manager->add_control( 'rtusa_footer_privacy_link', array(
            'label'   => __( 'Privacy Page to Link To', 'artsygeek' ),
            'section' => 'rtusa_footer_legal_link',
            'type'    => 'url',
            'priority' => 4,
            'sanitize_callback' => 'esc_url_raw'
        ) );
    }

    /**
     * Adds a new section to use custom controls in the WordPress customiser
     *
     * @param  Obj $wp_manager - WP Manager
     *
     * @return Void
     */
    private function custom_sections( $wp_manager )
    {
        $wp_manager->add_section( 'customiser_demo_custom_section', array(
            'title'          => 'Custom Controls Demo',
            'priority'       => 36,
        ) );

        // Add A Date Picker
        require_once dirname(__FILE__) . '/date/date-picker-custom-control.php';
        $wp_manager->add_setting( 'date_picker_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Date_Picker_Custom_Control( $wp_manager, 'date_picker_setting', array(
            'label'   => 'Date Picker Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'date_picker_setting',
            'priority' => 1
        ) ) );

        // Add A Layout Picker
        require_once dirname(__FILE__) . '/layout/layout-picker-custom-control.php';
        $wp_manager->add_setting( 'layout_picker_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Layout_Picker_Custom_Control( $wp_manager, 'layout_picker_setting', array(
            'label'   => 'Layout Picker Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'layout_picker_setting',
            'priority' => 2
        ) ) );

        // Add a category dropdown control
        require_once dirname(__FILE__) . '/select/category-dropdown-custom-control.php';
        $wp_manager->add_setting( 'category_dropdown_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Category_Dropdown_Custom_Control( $wp_manager, 'category_dropdown_setting', array(
            'label'   => 'Category Dropdown Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'category_dropdown_setting',
            'priority' => 3
        ) ) );

        // Add a menu dropdown control
        require_once dirname(__FILE__) . '/select/menu-dropdown-custom-control.php';
        $wp_manager->add_setting( 'menu_dropdown_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Menu_Dropdown_Custom_Control( $wp_manager, 'menu_dropdown_setting', array(
            'label'   => 'Menu Dropdown Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'menu_dropdown_setting',
            'priority' => 4
        ) ) );

        // Add a post dropdown control
        require_once dirname(__FILE__) . '/select/post-dropdown-custom-control.php';
        $wp_manager->add_setting( 'post_dropdown_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Post_Dropdown_Custom_Control( $wp_manager, 'post_dropdown_setting', array(
            'label'   => 'Post Dropdown Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'post_dropdown_setting',
            'priority' => 5
        ) ) );

        // Add a post type dropdown control
        require_once dirname(__FILE__) . '/select/post-type-dropdown-custom-control.php';
        $wp_manager->add_setting( 'post_type_dropdown_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Post_Type_Dropdown_Custom_Control( $wp_manager, 'post_type_dropdown_setting', array(
            'label'   => 'Post Type Dropdown Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'post_type_dropdown_setting',
            'priority' => 6
        ) ) );

        // Add a tags dropdown control
        require_once dirname(__FILE__) . '/select/tags-dropdown-custom-control.php';
        $wp_manager->add_setting( 'tags_dropdown_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Tags_Dropdown_Custom_Control( $wp_manager, 'tags_dropdown_setting', array(
            'label'   => 'Tags Dropdown Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'tags_dropdown_setting',
            'priority' => 7
        ) ) );

        // Add a taxonomy dropdown control
        require_once dirname(__FILE__) . '/select/taxonomy-dropdown-custom-control.php';
        $wp_manager->add_setting( 'taxonomy_dropdown_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Taxonomy_Dropdown_Custom_Control( $wp_manager, 'taxonomy_dropdown_setting', array(
            'label'   => 'Taxonomy Dropdown Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'taxonomy_dropdown_setting',
            'priority' => 8
        ) ) );

        // Add a user dropdown control
        require_once dirname(__FILE__) . '/select/user-dropdown-custom-control.php';
        $wp_manager->add_setting( 'user_dropdown_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new User_Dropdown_Custom_Control( $wp_manager, 'user_dropdown_setting', array(
            'label'   => 'User Dropdown Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'user_dropdown_setting',
            'priority' => 9
        ) ) );

        // Add a textarea control
        require_once dirname(__FILE__) . '/text/textarea-custom-control.php';
        $wp_manager->add_setting( 'textarea_text_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Textarea_Custom_Control( $wp_manager, 'textarea_text_setting', array(
            'label'   => 'Textarea Text Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'textarea_text_setting',
            'priority' => 10
        ) ) );

        // Add a text editor control
        require_once dirname(__FILE__) . '/text/text-editor-custom-control.php';
        $wp_manager->add_setting( 'text_editor_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Text_Editor_Custom_Control( $wp_manager, 'text_editor_setting', array(
            'label'   => 'Text Editor Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'text_editor_setting',
            'priority' => 11
        ) ) );

        // Add a Google Font control
        require_once dirname(__FILE__) . '/select/google-font-dropdown-custom-control.php';
        $wp_manager->add_setting( 'google_font_setting', array(
            'default'        => '',
        ) );
        $wp_manager->add_control( new Google_Font_Dropdown_Custom_Control( $wp_manager, 'google_font_setting', array(
            'label'   => 'Google Font Setting',
            'section' => 'customiser_demo_custom_section',
            'settings'   => 'google_font_setting',
            'priority' => 12
        ) ) );
    }

}